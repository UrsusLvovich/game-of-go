//package src.main.java;

import java.util.*;
import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel
{
	//I have no idea why I need this
	private static final long serialVersionUID = 1L;
	
   //Maybe find a way so that I don't have to have images folder in bin
   private ImageIcon white = new ImageIcon(Panel.class.getResource("images/white.jpg"));
   private ImageIcon black = new ImageIcon(Panel.class.getResource("images/black.jpg"));
   private ImageIcon blank = new ImageIcon(Panel.class.getResource("images/blank.JPG"));
   private ImageIcon BLCorner = new ImageIcon(Panel.class.getResource("images/BLCorner.jpg"));
   private ImageIcon TRCorner = new ImageIcon(Panel.class.getResource("images/TRCorner.jpg"));
   private ImageIcon TLCorner = new ImageIcon(Panel.class.getResource("images/TLCorner.jpg"));
   private ImageIcon BRCorner = new ImageIcon(Panel.class.getResource("images/BRCorner.jpg"));
   private ImageIcon TopEdge = new ImageIcon(Panel.class.getResource("images/TopEdge.jpg"));
   private ImageIcon LeftEdge = new ImageIcon(Panel.class.getResource("images/LeftEdge.jpg"));
   private ImageIcon RightEdge = new ImageIcon(Panel.class.getResource("images/RightEdge.jpg"));
   private ImageIcon BottomEdge = new ImageIcon(Panel.class.getResource("images/BottomEdge.jpg"));
   private ImageIcon Star = new ImageIcon(Panel.class.getResource("images/star.jpg"));

   private  final int SIZE=40;	//size of cell being drawn
   private int numRows=0;
   private int numColumns=0;
   private int PlayerBlackPoints=0; //Number of points for Player Black from capturing stones
   private int PlayerWhitePoints=0; //Number of points for  Player White from capturing stones
   private SparseMatrix<String> board = new SparseMatrix<String>(numRows, numColumns);  
   private int PxP;

   public Panel(int r, int c)
   {  
      numRows = r;
      numColumns = c;
      board = new SparseMatrix<String>(numRows,numColumns); 
   }

   //post:  shows different pictures on the screen in grid format depending on the values stored in the array board
   //			0-blank, 1-white, 2-black and gives priority to drawing the player
   public void showBoard(Graphics g)	
   {
      int x = 0, y=0;		//upper left corner location of where image will be drawn
      for(int r=1;r<=numRows;r++)
      {
         x = 0;						//reset the row distance
         for(int c=1;c<=numColumns;c++)
         {
            if(board.get(r,c)==null)
            {
               //check r,c coordinates
               if(r == 1 && c == 1)//TLCorner
               {
                  g.drawImage(TLCorner.getImage(), x, y, SIZE, SIZE, null);  //scaled image   
               }else if(r==1 && c==numColumns) //TRCorner
               {
                  g.drawImage(TRCorner.getImage(), x, y, SIZE, SIZE, null);  //scaled image
               }else if(r==numRows && c==1)//BLCorner
               { 
                  g.drawImage(BLCorner.getImage(), x, y, SIZE, SIZE, null);  //scaled image
               }else if(r==numRows && c==numColumns) //BRCorner
               {
                  g.drawImage(BRCorner.getImage(), x, y, SIZE, SIZE, null);  //scaled image
               }else if(r==1 && (c>1 && c<numColumns))//TopEdge
               {
                  g.drawImage(TopEdge.getImage(), x, y, SIZE, SIZE, null);  //scaled image
               }else if((r>1 && r<numRows) && c==numColumns) //RightEdge
               {
                  g.drawImage(RightEdge.getImage(), x, y, SIZE, SIZE, null);  //scaled image
               }else if((r>1 && r<numRows) && c==1) //LeftEdge
               {
                  g.drawImage(LeftEdge.getImage(), x, y, SIZE, SIZE, null);  //scaled image
               }else if(r==numRows && (c>1 && c<numColumns)){ //BottomEdge
                  g.drawImage(BottomEdge.getImage(), x, y, SIZE, SIZE, null);  //scaled image
               }else if((r==4 && c==4) || (r==4 && c==10) || (r==4 & c==16) || 
                        (r==10 && c==4) || (r==10 && c==10) || (r==10 && c==16) || 
                        (r==16 && c==4) || (r==16 && c==10) || (r==16 && c==16)) //Star, there's probably a better way to do this
               {
                  g.drawImage(Star.getImage(), x, y, SIZE, SIZE, null);  //scaled image
               }else
               {
                  g.drawImage(blank.getImage(), x, y, SIZE, SIZE, null);  //scaled image
               }
            }
            else
               if(board.get(r,c)=="o")
                  g.drawImage(white.getImage(), x, y, SIZE, SIZE, null);  //scaled image
               else // if(board[r][c]==2)
                  g.drawImage(black.getImage(), x, y, SIZE, SIZE, null);  //scaled image
            x+=SIZE;
         }
         y+=SIZE;
      }
      // System.out.println(board);
   }
   
   public void mouseChange(int r, int c, String piece)
   {
      r=(r/SIZE);
      c=(c/SIZE)+1;
      this.board.add(r,c,piece);
      
      //THIS IS BAD!!!
      //do not need to look at every piece on the board to see if it's captured
      //I only need to look at the piece played and the 4 surrounding pieces
      //capture method
      // for(int i=0;i<board.size();i++)
      //   for(int j=0;j<board.size();j++)
      //      capture(board,i,j,piece);
      
      capture(board,r,c,piece);
      capture(board,r+1,c,piece);
      capture(board,r,c+1,piece);
      capture(board,r-1,c,piece);
      capture(board,r,c-1,piece);
      //If the cell on the board is an empty space, I don't need to see if it's captured
      // if(!this.board.getCell(r+1,c).getValue().equals("-")){
      //    capture(board,r+1,c,piece);
      // }
      // if(!this.board.getCell(r,c+1).getValue().equals("-")){
      //    capture(board,r,c+1,piece);
      // }
      // if(!this.board.getCell(r-1,c).getValue().equals("-")){
      //    capture(board,r-1,c,piece);
      // }
      // if(!this.board.getCell(r,c-1).getValue().equals("-")){
      //    capture(board,r,c-1,piece);
      // }
   
      this.repaint();
   
   }
   
   public boolean isValid(int r, int c)
   {
      r=(r/SIZE);
      c=(c/SIZE)+1;
      if(this.board.get(r,c)==null)
         return true;
      return false;
   }

   @Override
public void paintComponent(Graphics g)
   {
      super.paintComponent(g); 
      g.setColor(Color.blue);		//draw a blue boarder around the board
      //g.fillRect(0, 0, ((numRows+1)*SIZE), ((numColumns+1)*SIZE));
      showBoard(g);					//draw the contents of the array board on the screen
   }

//pre: the row and column are within the board and the sparsematrix isn't null
//post: Determines whether or not a piece(s) can be captured. If it/they are, they are removed from the board. Returns the amount of pieces captured.
   public  void capture(SparseMatrix<String> board, int r, int c, String stone)
   {  
      //A Liberty is a vacant point that is immediately adjacent to a stone in a cardinal direction
      //A group of stones need at least 1 liberty to survive
      int numLiberties=0;
      ArrayList<Cell<String>> chainList = getChain(board,r,c,stone);
      for(int i=0;i<chainList.size();i++)
      {
         if(chainList.get(i).getValue().equals("-")){
            numLiberties++;
         }
      }
      System.out.println(String.format("Number of Liberties: %d",numLiberties));

      //If the number of liberties is 0, remove those stones
      if(numLiberties==0) {
         for(int i=0;i<chainList.size();i++)
         {
            board.remove(chainList.get(i).getRow(),chainList.get(i).getCol());
            
            // Test this funcationality
            if(chainList.get(i).getValue().equals("x")) {
               this.PlayerBlackPoints++;
            }else{
               if(chainList.get(i).getValue().equals("o")){
                  this.PlayerWhitePoints++;
               }
            }
         } 
      }
      //Just to see the current points on the board
      System.out.println(String.format("Black: %d\nWhite: %d",this.PlayerBlackPoints,this.PlayerWhitePoints));
   }
   
   
//pre: r and c are greater than zero, r and c are less than the amount of rows and columns on the board, and the sparsematrix isn't null
//post: Gets the objects above, below, to the right, and to the left of the object.
   @SuppressWarnings("unchecked")
public  ArrayList<Cell<String>> getNeighbors(SparseMatrix<String> a, int r, int c)
   {
      ArrayList<Cell<String>> neighbors = new ArrayList<Cell<String>>();
      neighbors.add(a.getCell(r-1,c));//top
      neighbors.add(a.getCell(r,c-1));//left
      neighbors.add(a.getCell(r,c+1));//right
      neighbors.add(a.getCell(r+1,c));//bottom
      return neighbors;
   }

//pre: none
//post: determine whether or not the two objects being sent are opposite
   public  boolean isOpposite(String a, String b)
   {
      if(a==null||b==null)
         return false;
      return !a.equals(b);
   }
   
   //pre: a is not null
   //post: checks if the array list is made up of only "-"
   public  boolean isClear(ArrayList<Cell<String>> a)
   {
      int temp=0;
      for(int i=0;i<a.size();i++)
         if(a.get(i).getValue().equals("-"))
            temp++;
      if(temp==a.size()&&a.size()>0)
         return true;
      return false;
   }
   
   //pre: arraylist size is greater than 0
   //post: searches through the array list and checks if piece is in it
   public  boolean search(ArrayList<Cell<String>> chain, Cell<String> piece)
   {
      if(piece==null)
         return false;
      for(int i=0;i<chain.size();i++)
      {
         Cell<String> current = chain.get(i);
         if(piece.getValue()==current.getValue() && piece.getRow()==current.getRow()&& piece.getCol()==current.getCol())
            return true;
      }
      return false;
   }

//pre: the sparsematrix isn't null
//post: helper method to the getChain method
   public  void getChainHelper(SparseMatrix<String> board, int r, int c, String value, ArrayList<Cell<String>> chain)
   {
      if(r<=0||c<=0||r>board.getNumRow()||c>board.getNumCol())
         return;
      Cell<String> piece = board.getCell(r,c);
      if(piece==null)
         piece = new Cell<String>("-", r, c, r*board.getNumCol()+c);
      
      //If the piece is already in the chain, end the search for this piece.
      if(search(chain,piece)) {
         return;
      }
      
      //If Value and Piece are the same colored stone, or Piece is an empty space
      if(value.equals(piece.getValue().trim()) || piece.getValue().equals("-"))
      {
         chain.add(piece);
         if(!piece.getValue().equals("-")){
            getChainHelper(board,r-1,c,value,chain);
            getChainHelper(board,r+1,c,value,chain);
            getChainHelper(board,r,c-1,value,chain);
            getChainHelper(board,r,c+1,value,chain);
         }
      }
   } 

//pre: the sparsematrix isn't null
//post: Creates an arraylist of values that are the same and are next to each other. It also puts in any spaces that are next to those values as well.
   public  ArrayList<Cell<String>> getChain(SparseMatrix<String> board, int r, int c, String value)
   {
      ArrayList<Cell<String>> chain = new ArrayList<Cell<String>>();
      getChainHelper(board,r,c,value,chain);
      for(int i=0;i<chain.size();i++)
         if(chain.get(i).equals("-"))
            return null;
      return chain; 
   }

}
