import java.io.*;
import java.util.*;
import javax.swing.*;

public class GoDriver
{
   
   public static Scanner input = new Scanner(System.in);
      
   //pre: r and c are greater than zero, r and c are less than the amount of rows and columns on the board, and the sparsematrix isn't null
   //post: Gets the objects above, below, to the right, and to the left of the object.
   public static ArrayList<Cell<String>> getNeighbors(SparseMatrix<String> a, int r, int c)
   {
      ArrayList<Cell<String>> neighbors = new ArrayList();
      neighbors.add(a.getCell(r-1,c));//top
      neighbors.add(a.getCell(r,c-1));//left
      neighbors.add(a.getCell(r,c+1));//right
      neighbors.add(a.getCell(r+1,c));//bottom
      return neighbors;
   }

   //pre: none
   //post: determine whether or not the two objects being sent are opposite
   public static boolean isOpposite(String a, String b)
   {
      if(a==null||b==null)
         return false;
      return !a.equals(b);
   }
      
   //pre: a is not null
   //post: checks if the array list is made up of only "-"
   public static boolean isClear(ArrayList<Cell<String>> a)
   {
      int temp=0;
      for(int i=0;i<a.size();i++)
         if(a.get(i).getValue().equals("-"))
            temp++;
      if(temp==a.size()&&a.size()>0)
         return true;
      return false;
   }
      
   //pre: arraylist size is greater than 0
   //post: searches through the array list and checks if piece is in it
   public static boolean search(ArrayList<Cell<String>> a, Cell<String> piece)
   {
      if(piece==null)
         return false;
      for(int i=0;i<a.size();i++)
      {
         Cell<String> current = a.get(i);
         if(piece.getValue()==current.getValue() && piece.getRow()==current.getRow()&& piece.getCol()==current.getCol())
            return true;
      }
      return false;
   }

   //pre: the sparsematrix isn't null
   //post: helper method to the getChain method
   public static void getChainHelper(SparseMatrix<String> a, int r, int c, String value, ArrayList<Cell<String>> chain)
   {
      if(r<=0||c<=0||r>a.getNumRow()||c>a.getNumCol())
         return;
      Cell<String> piece = a.getCell(r,c);
      if(piece==null)
         piece = new Cell("-", r, c, r*a.getNumCol()+c);
      
      if((value.equals(piece.getValue().trim()) || piece.getValue().equals("-")) && !search(chain, piece))
      {
         chain.add(piece);
         getChainHelper(a,r-1,c,value,chain);
         getChainHelper(a,r+1,c,value,chain);
         getChainHelper(a,r,c-1,value,chain);
         getChainHelper(a,r,c+1,value,chain);
      }
   } 

   //pre: the sparsematrix isn't null
   //post: Creates an arraylist of values that are the same and are next to each other. It also puts in any spaces that are next to those values as well.
   public static ArrayList<Cell<String>> getChain(SparseMatrix<String> a, int r, int c, String value)
   {
      ArrayList<Cell<String>> chain = new ArrayList();
      getChainHelper(a,r,c,value,chain);
      for(int i=0;i<chain.size();i++)
         if(chain.get(i).equals("-"))
            return null;
      return chain; 
   }


   //pre: the row and column are within the board and the sparsematrix isn't null
   //post: Determines whether or not a piece(s) can be captured. If it/they are, they are removed from the board. Returns the amount of pieces captured.
   public static int capture(SparseMatrix<String> a, int r, int c, String value, int PxP)
   {
      int temp=0, ans=0;
      ArrayList<Cell<String>> chainList = getChain(a,r,c,value);
      for(int i=0;i<chainList.size();i++)
      {
         if(chainList.get(i).getValue().equals("-"))
            temp++;
      }
      if(temp<1)
         for(int i=0;i<chainList.size();i++)
         {
            PxP++;
            a.remove(chainList.get(i).getRow(),chainList.get(i).getCol());
         }
      return PxP;
   }
      
   //pre: the sparse matrix and value aren't null
   //post: calculates how many points PxP has by the amount of pieces they have captured
   public static int getCapturePoints(SparseMatrix<String> a, String value, int PxP)
   {
      for(int r=1;r<=a.size();r++)
         for(int c=1;c<=a.size();c++)
            PxP=capture(a,r,c,value,PxP);
            
      return PxP;
   }

   //pre: the sparse matrix isn't null, value is x or o
   //post: calculates how many points PxP has by the amount of territory they have
   public static int getTerritoryPoints(SparseMatrix<String> a, String value, int PxP)
   {
      ArrayList temp = null;
      for(int r=1;r<=a.size();r++)
         for(int c=1;c<=a.size();c++)
         {
            temp=getChain(a,r,c,value);
            if(isClear(temp))
               PxP++;
         }
      
      return PxP;
   }
      
   public static void main(String argv[]) throws IOException
   {
      Board board = new Board();
      board.screen = new Panel(19,19);
      JFrame frame = new JFrame("Go");	//window title
      frame.setSize(800, 800);					//Size of game window
      frame.setLocation(0, 0);				//location of game window on the screen
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      board.screen.repaint();
      frame.setContentPane(board.screen);		
      frame.setVisible(true);
      frame.addMouseListener(new Board.listen());
   }
    
}