//package main.java;



public class Cell<anyType>
{
   private int row,col,key;
   private anyType value;
   
   public Cell()
   {
      value=null;
      row=0;
      col=0;
      key=0;
   }  
   
   public Cell(anyType a, int r, int c, int k)
   {
      value=a;
      row=r;
      col=c;
      key=k;
   }
   
   public int getRow()
   {
      return row;
   }

   public int getCol()
   {
      return col;
   }

   public int getKey()
   {
      return key;
   }
   
   public anyType getValue()
   {
      return value;
   }
   
   @Override
public String toString()
   {
      return value.toString();
   }
   
}