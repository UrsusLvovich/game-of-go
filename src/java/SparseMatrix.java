//package src.main.java;

import java.util.*;

   public class SparseMatrix<anyType>
   {
      private ArrayList<Cell<anyType>> list;
      private int numRow;
      private int numCol;
   
      public SparseMatrix()
      {
         numRow=0;
         numCol=0;
         list=new ArrayList();
      }
   
      public SparseMatrix(int r, int c)
      {
         numRow=r;
         numCol=c;
         list=new ArrayList();
      }
   
      public Cell getCell(int r, int c)
      {
         int key = numCol*r+c;
         for(int i=0;i<list.size();i++)
         {
            if(list.get(i).getKey()==key)
               return list.get(i);
         }
         return null;
      }
      
      public anyType get(int r, int c)
      {
         int key = numCol*r+c;
         for(int i=0;i<list.size();i++)
         {
            if(list.get(i).getKey()==key)
               return list.get(i).getValue();
         }
         return null;
      }
   
   	
      public void add(int r, int c, anyType x)
      {
         int key = numCol*r+c;
         if(r<0||c<0||r>numRow||c>numCol)
         {
            return;
         }
         for(int i=0;i<list.size();i++)
         {
            if(list.get(i).getKey()==key)
               return;
            if(list.get(i).getKey()>key)
            {
               list.add(i, new Cell(x,r,c,key));
               return;   
            }
         }
         list.add(new Cell(x,r,c,key));
      }
   
      public boolean isEmpty(int r, int c)
      {
         int key=numCol*r+c;
         for(int i=0;i<list.size();i++)
            if(list.get(i).getKey()==key)
               return false;
         return true;
      }
   
      public int getNumCol()
      {
         return numCol;
      }
   
      public int getNumRow()
      {
         return numRow;
      }
   
      public int size()
      {
         return list.size();
      }
   
      public void remove(int r, int c)
      {
         int key = numCol*r+c;
         for(int i=0;i<list.size();i++)
            if(list.get(i).getKey()==key)
               list.remove(i);
      }
   
      @Override
	public String toString()
      {
         String ans = "";
         for(int i=1;i<=numCol;i++)
         {
            if(i==1)
            {
               System.out.print("   ");
            }
            if(i<10)
               System.out.print("0"+i+" ");
            else
               System.out.print(i+" ");
         }
         System.out.println();
         for(int r=1; r<=numRow; r++)
         {
            if(r<10)
               ans+=("0"+r+" ");
            else
               ans+=(r+" ");
            for(int c=1; c<=numCol; c++)
            {
               Cell<anyType> current = this.getCell(r,c);
               if(current==null)
                  ans+=("-  ");
               else
                  ans+=(current+ " ");
            }
            ans+="\n";
         }		
         return ans;
      }
   }
