import java.util.*;
import java.awt.event.*;

public class Board								//Driver Program
{
   public static Panel screen;					//Game window
   public static int turn = 0;
   public static Scanner input = new Scanner(System.in);

   public static class listen implements MouseListener
   {
      @Override
      public void mouseExited(MouseEvent e)
      {
      
      }
      
      @Override
      public void mouseEntered(MouseEvent e)
      {
      
      }
      
      @Override
      public void mouseReleased(MouseEvent e)
      {
         int r = e.getY();
         int c = e.getX();
         if(c>760||!screen.isValid(r,c))
            return;
         String piece="";
         if(turn%2 == 0)
            piece = "x";
         if(turn%2 == 1)
            piece = "o"; 
         turn++;
         screen.mouseChange(r,c,piece);
      }  
      
      @Override
      public void mouseClicked(MouseEvent e)
      {
      
      }

      @Override
      public void mousePressed(MouseEvent e)
      {
      
      }
   }
}
